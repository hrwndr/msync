document.querySelector('#searchBtn').addEventListener('click', () => {
	document.querySelector('#mainNav').style.display = "none";
	document.querySelector('.searchNav').style.display = "block";
	document.querySelector('.navbar-fixed').classList.remove("navbar-fixed");
});

document.querySelector('.search-close').addEventListener('click', () => {
	document.querySelector('#mainNav').style.display = "block";
	document.querySelector('.searchNav').style.display = "none";
	document.querySelector('.domnav').classList.add("navbar-fixed");
	document.querySelector('#search').value = '';
});

//Audio Plaer
$(document).ready(() => {
	$('video').mediaelementplayer({
		features: ['playpause','progress','current','duration','tracks','volume'],
		youtube: {imageQuality: "sddefault"},
		enableProgressTooltip: true,
		useSmoothHover: true
	});
});

//SideNAv   
$(document).ready(function(){
	$('.sidenav').sidenav();
});
        

//Search And Fetch
document.querySelector('#search').addEventListener('input', e => {
	document.querySelector('#song-list-title').innerHTML = 'Search Results'
	e.preventDefault();
	document.querySelector('#songList').innerHTML = '';
	let query = document.querySelector('#search').value;
	let ytapi = `https://www.googleapis.com/youtube/v3/search?part=snippet%2C+id&order=viewCount&q=${query}&maxResults=30&type=video&videoDefinition=high&key=AIzaSyBlOO1MKHNapWIgQdz8TDGO496c8MxFU_g`;
	fetch(ytapi).then(res => res.json()).then(data => {
		data.items.forEach((n, i) => {
		document.querySelector('#songList').innerHTML += `
			<li><div class="row">
		    <div class="col s12 m12">
		      <div class="card waves-effect waves-light card-style" onclick="playNow('${n.id.videoId}')">
			<div class="col s3 m3">
				<img src="${n.snippet.thumbnails.default.url}" width="85px" alt="Album Art"/>
			</div>
			<div class="col s9 m9">
		        <div class="card-content white-text">
		          <span class="flow-text">${n.snippet.title}</span>
		          <p>${n.snippet.channelTitle}</p>
		        </div>
		     	</div>
		      </div>
		    </div>
		  </div></li>
		`;
	});
}).catch(err => console.log(err));
});

// Fetch Songs
let ytapi = `https://www.googleapis.com/youtube/v3/search?part=snippet%2C+id&order=viewCount&q=punjabi+songs&maxResults=10&type=video&videoDefinition=high&key=AIzaSyBlOO1MKHNapWIgQdz8TDGO496c8MxFU_g`;
	fetch(ytapi).then(res => res.json()).then(data => {
		data.items.forEach((n, i) => {
		document.querySelector('#songList').innerHTML += `
			<li><div class="row">
		    <div class="col s12 m12">
		      <div class="card waves-effect waves-light card-style" onclick="playNow('${n.id.videoId}')">
			<div class="col s3 m3">
				<img src="${n.snippet.thumbnails.default.url}" width="85px" alt="Album Art"/>
			</div>
			<div class="col s9 m9">
		        <div class="card-content white-text">
		          <span class="flow-text">${n.snippet.title}</span>
		          <p>${n.snippet.channelTitle}</p>
		        </div>
		     	</div>
		      </div>
		    </div>
		  </div></li>
		`;
	});
	}).catch(err => console.log(err));

	//Play Function
playNow = vidId => {
	let player = document.querySelector('#songplayer');
	player.src = 'https://www.youtube.com/watch?v='+vidId;

	db.ref().child('ranid/'+cookieVal[1]).update({
		songurl: 'https://www.youtube.com/watch?v='+vidId
	});
}
/**
 * Check out https://googlechromelabs.github.io/sw-toolbox/ for
 * more info on how to use sw-toolbox to custom configure your service worker.
 */


'use strict';
importScripts('./sw-toolbox.js');

self.toolbox.options.cache = {
  name: 'msync-cache'
};

self.toolbox.precache(
  [
    'style.css',
    'app.js',
    'design.js',
    'manifest.json'
  ]
);

self.toolbox.router.any('/*', self.toolbox.fastest);

self.toolbox.router.default = self.toolbox.networkFirst;

const config = {
  apiKey: "AIzaSyDoS7dsa_8EmAz80s-k16atUdK6H7E-Zlc",
  authDomain: "pr-musicapp.firebaseapp.com",
  databaseURL: "https://pr-musicapp.firebaseio.com",
  projectId: "pr-musicapp",
  messagingSenderId: "684549506022"
};
firebase.initializeApp(config);

let db = firebase.database();

let cookieVal = document.cookie.split("id=");

if(document.cookie.startsWith("maid")) {

  console.log(cookieVal[1]);
  document.querySelector('.unid').innerHTML = '<br/>&nbsp;&nbsp;'+cookieVal[1];

  // Play song from database
  db.ref().child('ranid/'+cookieVal[1]).on('value', snap => {
    if(snap.val().playing == 1) {
      document.querySelector('#songplayer').src = snap.val().songurl;

          console.log(document.querySelector('#songplayer').getCurrentTime());
         let audioTag = document.querySelector('#songplayer');
         
      audioTag.addEventListener('timeupdate', () => {

          let ctime = document.querySelector('#songplayer').getCurrentTime();
        // console.log(ctime);
        db.ref().child(`ranid/${cookieVal[1]}CurTime`).update({
          curSongTime: ctime
        }).catch(err => console.log("Error: ", err));

      });



    } else if(snap.val().playing == 0) {
      // document.querySelector('#play').innerHTML = '<button onclick="startPlaying()">Play</button>';
    }
  }); 
} else {
  db.ref().child('ranid').push({
    songurl: document.querySelector('#songplayer').src.toString(),
    playing: 1,
    curSongTime: 0
  }).then(res => {
    document.cookie = "maid="+res.getKey();
    location.reload();
  }).catch(err => console.log("Error: ", err));
}

document.querySelector('.enter-unique-id').addEventListener('submit', e => {
  e.preventDefault();
  let id = document.querySelector('#idfield').value;
  document.querySelector('.synced-with').innerHTML = '<br/>&nbsp;Synced with: '+id;

      db.ref().child('ranid/'+id).on('value', idSnap => {
    db.ref().child('ranid/'+cookieVal[1]).update({
      playing: idSnap.val().playing,
      songurl: idSnap.val().songurl,
    });

    db.ref().child('ranid/'+id+'CurTime').once('value', Snap => {

        let audioTag = document.querySelector('#songplayer');
        audioTag.setCurrentTime(Snap.val().curSongTime);

      db.ref().child('ranid/'+cookieVal[1]+'CurTime').set({
        curSongTime: Snap.val().curSongTime
      }).then(async () => await console.log('Synced With '+id));
    });
  })
})